from django.db import models

# Create your models here.


class FileModel(models.Model):
    title = models.CharField(max_length=11)
    picture = models.FileField(upload_to='picture')
