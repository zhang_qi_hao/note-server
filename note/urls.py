from django.urls import path

from . import views

urlpatterns = [
    path('add/', views.add_note),
    path('list/', views.list_note),
    path('update/', views.update_note),
    path('delete/', views.delete_note),
    path('detail/', views.detail_view),
]