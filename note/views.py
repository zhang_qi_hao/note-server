from django.http import JsonResponse, HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from .models import Note

# Create your views here.


def check_login(fn):
    def wrap(request, *args, **kwargs):
        if 'username' not in request.session or 'uid' not in request.session:
            cookie_username = request.COOKIES.get('username')
            cookie_uid = request.COOKIES.get('uid')
            if not cookie_username or not cookie_uid:
                return HttpResponseRedirect('/user/login')
            else:
                request.session['username'] = cookie_username
                request.session['uid'] = cookie_uid
        return fn(request, *args, **kwargs)
    return wrap

@check_login
def add_note(request):
    res = {}
    if request.method == 'GET':
        return render(request, 'note/add_note.html')
    elif request.method == 'POST':
        uid = request.session.get('uid')
        title = request.POST.get('title')
        content = request.POST.get('content')
        Note.objects.create(title=title, content=content, user_id=uid)

        res['code'] = 200
        res['msg'] = '添加成功'
        return JsonResponse(res)
    else:
        return render(request, '404.html')


@check_login
def list_note(request):
    res = {}
    res['code'] = 0
    res['msg'] = '请求成功'
    res['count'] = 3
    res['data'] = [
        {
            'title': '1111',
            'id': 1,
            'created_time': '2022-08-18 14:49:37.320475',
            'update_time': '2022-08-18 14:49:37.320475',

        },
        {
            'title': '2222',
            'id': 2,
            'created_time': '2022-08-18 14:49:37.320475',
            'update_time': '2022-08-18 14:49:37.320475',

        },
        {
            'title': '3333',
            'id': 3,
            'created_time': '2022-08-18 14:49:37.320475',
            'update_time': '2022-08-18 14:49:37.320475',

        },
    ]
    return JsonResponse(res)


@check_login
def update_note(request):
    if request.method == 'GET':
        note_id = request.GET.get('id')

        try:
            note = Note.objects.get(id=note_id)
            data = {
                'id': note_id,
                'title': note.title,
                'content': note.content
            }
            return render(request, 'note/update_note.html', context=data)
        except Exception as e:
            print(f'--Query note erroe {e}')
            return JsonResponse({'code': 500, 'message': "修改失败"})
    if request.method == 'POST':
        note_id = request.POST.get('id')
        title = request.POST.get('title')
        content = request.POST.get('content')
        res = {}
        try:
            Note.objects.filter(id=note_id).update(title=title, content=content)
            res['code'] = 200
            res['msg'] = '修改成功'
        except Exception as e:
            print(f'--Update note error {e}')
            res['code'] = 500
            res['msg'] = '修改失败'
        return JsonResponse(res)

@check_login
def delete_note(request):
    if request.method == 'POST':
        note_id = request.POST.get('id')
        res = {}
        try:
            Note.objects.filter(id=note_id).delete()
            res['code'] = 200
            res['msg'] = '删除成功'

        except Exception as e:
            print(f'--Delete note error {e}')
            res['code'] = 500
            res['msg'] = '删除失败'
        return JsonResponse(res)
    else:
        return HttpResponse('Hello World!')

@check_login
def detail_view(request):
    if request.method == 'GET':
        note_id = request.GET.get('id')
        try:
            note = Note.objects.get(id=note_id)
            data = {
                'title': note.title,
                'content': note.content,
            }
            return render(request, 'note/detail.html',  context=data)

        except Exception as e:
            print(f'--Show note error {e}')
            return render(request, '404.html')
    else:
        return HttpResponse('Hello World!')